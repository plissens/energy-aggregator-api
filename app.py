from flask import Flask, jsonify
import requests
import json

from requests.sessions import to_native_string
import sbModBus
import math

app = Flask(__name__)

settings = {}
with open("settings.json") as settingFile:
    settings = json.loads(settingFile.read())

mappings = {}
with open("mappings.json") as mappingFile:
    mappings = json.loads(mappingFile.read())

@app.route("/aggregate")
def aggregate():
    p1Data = requests.get("http://" + settings["p1"] + "/api/v1/data").json()
    sunnyboyData = {}
    for sunnyBoy in settings["sunnyBoys"]:
        sunnyBoyClient = sbModBus.sbModBus(sunnyBoy, settings["dataTypes"])
        for dataType in mappings:
            value = sunnyBoyClient.getRegister(mappings[dataType]["reg"], mappings[dataType]["type"])
            if not (mappings[dataType]["scaling"] == 0):
                value = value * math.pow(10, sunnyBoyClient.getRegister(mappings[dataType]["scaling"], mappings[dataType]["scaling_type"]))
            if value < 0:
                value = 0
            try:
                sunnyboyData[sunnyBoy["name"]] = {**sunnyboyData[sunnyBoy["name"]],  dataType: {**mappings[dataType], "value" : value}}
            except KeyError:
                sunnyboyData = {**sunnyboyData, sunnyBoy["name"]:{ dataType: {**mappings[dataType], "value" : value}}}
    for dataType in mappings:
        total = 0
        for sunnyBoy in settings["sunnyBoys"]:
            total = total + sunnyboyData[sunnyBoy["name"]][dataType]["value"]
        try:
            sunnyboyData["total"] = {**sunnyboyData["total"], dataType: {**mappings[dataType], "value" : total}}
        except KeyError:
            sunnyboyData = {**sunnyboyData, "total":{ dataType: {**mappings[dataType], "value" : total}}}

    data = {"p1Data" : p1Data, "sunnyboyData":  sunnyboyData}
    return jsonify(data)

