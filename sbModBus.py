from pyModbusTCP.client import ModbusClient
from pyModbusTCP import utils

class sbModBus:
    def __init__(self, sunnyBoy, dataTypes):
        self.name = sunnyBoy['name']
        self.dataTypes = dataTypes
        try:
            self.sbClient = ModbusClient(host=sunnyBoy['host'], port=sunnyBoy['port'], auto_open=True, auto_close=True)
            self.sbClient.unit_id(126)
            self.sbClient.debug(False)
        except ValueError:
            #TODO: make logging??
            print("Error with host or port params")
    def getRegister(self, reg, type):
        raw = self.sbClient.read_input_registers(reg,self.dataTypes[type])
        match type:
            case "uint32":
                return utils.word_list_to_long(raw)[0]
            case "uint16":
                return raw[0]
            case "int16":
                return utils.get_2comp(raw[0])
            case "acc64":
                return utils.word_list_to_long(raw, long_long=True)[0]
            case "acc32":
                return utils.word_list_to_long(raw)[0]
            case _:
                raise KeyError("cannot convert type " + type + " yet")
